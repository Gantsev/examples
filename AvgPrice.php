<?php declare(strict_types=1);

namespace App\Orders\Service;

/**
 * Class AvgPrice
 *
 * @package App\Orders\Service
 */
class AvgPrice
{
    /** @var \App\Orders\Dao\AvgPrice */
    private $dao;

    /**
     * Сколько процентов прибавить к рекомендуемой ставке, в зависимости от среднего кол-ва страниц
     *
     * @see AvgPrice::getMultiplier()
     * @var array map[int]float
     */
    private $multipliers = [
        5  => 0.3,
        10 => 0.5,
        20 => 0.6,
    ];

    /**
     * Максимальный множитель
     *
     * @var float
     */
    private $maxMultiplier = 1.0;

    /**
     * Тип работы
     *
     * @var int
     */
    private $typeId = 0;

    /**
     * Категория (тема) работы
     *
     * @var int
     */
    private $categoryId = 0;

    /**
     * Минимальное кол-во страниц
     *
     * @var int
     */
    private $minPage = 0;

    /**
     * Максимальное кол-во страниц
     *
     * @var int
     */
    private $maxPage = 0;

    public function __construct()
    {
        $this->dao = new \App\Orders\Dao\AvgPrice();
    }

    /**
     * Установка типа работы
     *
     * @param int $id
     *
     * @return AvgPrice
     * @access
     */
    public function setType(int $id): self
    {
        $this->typeId = $id;

        return $this;
    }

    /**
     * Установка категории работы
     *
     * @param int $id
     *
     * @return AvgPrice
     * @access
     */
    public function setCategory(int $id): self
    {
        $this->categoryId = $id;

        return $this;
    }

    /**
     * Установка страниц
     *
     * @param int $min
     * @param int $max
     *
     * @return AvgPrice
     * @access
     */
    public function setPages(int $min, int $max): self
    {
        $this->minPage = $min;
        $this->maxPage = $max;

        return $this;
    }

    /**
     * Получение рекомендованной цены
     *
     * @return int|null
     * @access
     */
    public function getPrice(): ?int
    {
        $avg = $this->dao->getAvgRecommendedPrice($this->typeId, $this->categoryId);

        return $avg ? intval($avg + ceil($avg * $this->getMultiplier())) : null;
    }

    /**
     * На сколько нужно увеличить рекомендованную цену, в зависимости от среднего кол-ва страниц
     *
     * @return float
     * @access
     */
    private function getMultiplier(): float
    {
        $avgPages = ($this->minPage + $this->maxPage) / 2;

        // Больше, чем макс. в шкале - сразу лупим по полной
        if ($avgPages >= max(array_keys($this->multipliers))) {
            return $this->maxMultiplier;
        }

        ksort($this->multipliers, SORT_NUMERIC);

        foreach ($this->multipliers as $avg => $multiplier)
        {
            // Нашли нужное нам значение min < x < max
            if ($avgPages < $avg) {
                return $multiplier;
            }
        }

        return 0.0;
    }

    /**
     * Запускается по крону
     *
     * Записывает значения средней цены для пары тип:категория в редис
     *
     * @access
     */
    public function writeBidsToRedis(): void
    {
        \Base_Registry::set(USESLAVEFORSELECT, true);

        $data = $this->dao->getAvgBidsOverPeriod();

        foreach ($data as $item)
        {
            if (is_null($item->avg_bid))
            {
                continue;
            }

            $this->dao->saveAvgPriceToRedis($item->type_id, $item->category_id, intval($item->avg_bid));
        }
    }
}