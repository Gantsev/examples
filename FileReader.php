<?php declare(strict_types = 1);

/**
 * Задача 3
 * Дан текстовый файл размером 2ГБ.
 * Напишите класс, реализующий интерфейс SeekableIterator, для чтения данного файла
 */

$start = time();

/**
 * Class FileReader
 *
 * @property resource $handler
 * @property array $positions
 * @property integer $currentPosition
 * @property integer $currentBitPosition
 * @property integer $maxPosition
 */
class FileReader implements SeekableIterator
{
    /**
     * @var resource
     */
    private $handler;
    /**
     * Строка => начало в байтах
     * @var GMP
     */
    private $positions;
    /**
     * Текущая позиция
     * @var int
     */
    private $currentPosition = 0;
    /**
     * Текущая поцизия бита 1
     * @var int
     */
    private $currentBitPosition = 0;

    /**
     * Последняя строка
     * @var int
     */
    private $maxPosition = 0;

    /**
     * FileReader constructor
     *
     * @param string $filename
     *
     * @throws Exception
     */
    public function __construct(string $filename)
    {
        if (is_file($filename)) {
            $this->positions = gmp_init(0, 2);
            $this->handler = fopen($filename, 'r');
            while (false !== ($str = fgets($this->handler, 1024))) {
                if (preg_match('/[\r\n]$/u', $str)) {
                    $pos = ftell($this->handler);
                    gmp_setbit($this->positions, $pos, true);
                    ++$this->maxPosition;
                }
            }
        } else {
            throw new Exception('File not exists');
        }
    }

    /**
     * Вывести текущую строку
     *
     * @return string
     *
     * @throws Exception
     *
     * @access public
     */
    public function current() : string
    {
        if ($this->currentPosition == 0) {
            fseek($this->handler, 0);
        } elseif (gmp_testbit($this->positions, $this->currentBitPosition)) {
            fseek($this->handler, $this->currentBitPosition);
        } else {
            throw new Exception("invalid current position ({$this->currentPosition})");
        }

        return fgets($this->handler);
    }

    /**
     * Индекс текущей строки
     *
     * @return int
     *
     * @access public
     */
    public function key() : int
    {
        return $this->currentPosition;
    }

    /**
     * Двигаемся на след. строку
     *
     * @access public
     */
    public function next()
    {
        ++$this->currentPosition;
        $this->currentBitPosition = gmp_scan1($this->positions, $this->currentBitPosition + 1);
    }

    /**
     * Сбрасываемся в начало файла
     *
     * @access public
     */
    public function rewind()
    {
        $this->currentPosition = 0;
        $this->currentBitPosition = 0;
    }

    /**
     * Устанавливаем курсор на нужную строку
     *
     * @param int $position
     *
     * @throws Exception
     *
     * @access public
     */
    public function seek($position)
    {
        settype($position, 'integer');

        if ($position < $this->maxPosition) {

            $this->rewind();

            if ($position > 0) {
                $i = 0;
                while ($position > $i) {
                    $this->next();
                    ++$i;
                }
            }
        } else {
            throw new Exception("invalid seek position ({$position})");
        }
    }

    /**
     * Мы не заблудились?
     *
     * @return bool
     *
     * @access public
     */
    public function valid() : bool
    {
        return gmp_testbit($this->positions, $this->currentBitPosition);
    }
}

/*
if (is_file('/tmp/tmp.txt')) {
    unlink('/tmp/tmp.txt');
}

for ($i = 0; $i < 10000000; ++$i) {
    file_put_contents('/tmp/tmp.txt', $i . ' ' . md5((string)rand()) . PHP_EOL, FILE_APPEND);
}
*/

$Iterator = new FileReader('/tmp/tmp.txt');

$Iterator->seek(537893);
echo $Iterator->current();
$Iterator->seek(687494);
echo $Iterator->current();
$Iterator->rewind();
echo $Iterator->current();
$Iterator->next();
echo $Iterator->current();
$Iterator->seek(8645764);
echo $Iterator->current();
$Iterator->next();
echo $Iterator->current();
echo PHP_EOL;
echo memory_get_peak_usage();
echo PHP_EOL;
echo time() - $start;
