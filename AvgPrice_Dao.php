<?php declare(strict_types=1);

namespace App\Orders\Dao;

/**
 * Средняя рекомендованная цена (для заказчика)
 *
 * Class AvgPrice
 *
 * @package App\Orders\Dao
 */
class AvgPrice
{
    /** Где храним */
    private const REDIS_KEY = 'Orders:AvgPriceByTypeAndCategory';

    /** Шаблон для индекса хэша */
    private const MEMBER_TPL = '%d:%d';

    /** За какой период считаем среднюю цену (в месяцах) */
    private const PERIOD_MONTHS = -6;

    /**
     * Получем рекомендованную цену из редиса для типа и категории
     *
     * @param int $typeId
     * @param int $categoryId
     *
     * @return int|null
     * @access
     */
    public function getAvgRecommendedPrice(int $typeId, int $categoryId): ?int
    {
        $avg = \Base_Service_Redis::hget(__METHOD__, self::REDIS_KEY, $this->getMemberKey($typeId, $categoryId));

        return $avg ? (int)$avg : null;
    }

    /**
     * Ключик :)
     *
     * @param int $typeId
     * @param int $categoryId
     *
     * @return string
     * @access
     */
    private function getMemberKey(int $typeId, int $categoryId): string
    {
        return sprintf(self::MEMBER_TPL, $typeId, $categoryId);
    }

    /**
     * Получает средние ставки за месяц по типам и категориям
     *
     * @return iterable
     * @access
     */
    public function getAvgBidsOverPeriod(): iterable
    {
        return \EloquentHook::getQueryBuilder()
            ->table('info')
            ->selectRaw('avg("origin_bid") AS avg_bid, "type_id", "category_id"')
            ->join('dates', 'dates.order_id', '=', 'info.order_id')
            ->where('creation', '>', \Base_Date::create()->modifyMonths(self::PERIOD_MONTHS)->formatSql())
            ->groupBy('type_id', 'category_id')
            ->get();
    }

    /**
     * Сохраняем данные
     *
     * @param int $typeId
     * @param int $categoryId
     * @param int $avgPrice
     *
     * @access
     */
    public function saveAvgPriceToRedis(int $typeId, int $categoryId, int $avgPrice): void
    {
        \Base_Service_Redis::hset(__METHOD__, self::REDIS_KEY, $this->getMemberKey($typeId, $categoryId), $avgPrice);
    }
}