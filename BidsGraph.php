<?php declare(strict_types=1);

namespace App\Orders\Service;

/**
 * График ставок
 *
 * Class BidsGraph
 *
 * @package App\Orders\Service
 */
class BidsGraph
{
    /**
     * За какой период делать выборку при добавлении
     */
    private const PERIOD_MONTHS = -12;

    /**
     * Множитель среднего значения
     */
    private const AVG_MULTIPLIER = 2;

    /**
     * Минимальный процент, для расчёта мин и макс от среднего
     */
    private const MIN_PERCENT = 10;

    /**
     * Максимальный процент, для расчёта мин и макс от среднего
     */
    private const MAX_PERCENT = 20;

    /** Где кэшируем */
    private const REDIS_KEY = 'BidsGraph:cache';

    /** Шаблон для индекса хэша */
    private const MEMBER_TPL = '%d:%d';

    /**
     * Ключик :)
     *
     * @param int $typeId
     * @param int $categoryId
     *
     * @return string
     * @access
     */
    private static function getMemberKey(int $typeId, int $categoryId): string
    {
        return sprintf(self::MEMBER_TPL, $typeId, $categoryId);
    }

    /**
     * Получает значения ставок для графика
     *
     * @param int $typeId
     * @param int $categoryId
     *
     * @return array
     * @access
     */
    public static function getBids(int $typeId, int $categoryId): array
    {
        // { date: { month: 5, year: 2016 }, min: 885, max: 5547 },
        $result = [];

        $memberKey = self::getMemberKey($typeId, $categoryId);
        $data = \Base_Service_Redis::hget(__METHOD__, self::REDIS_KEY, $memberKey);

        if (!$data)
        {
            $data = \Models\BidsGraph::findByTypeAndCategory($typeId, $categoryId)->toArray();

            if ($data)
            {
                \Base_Service_Redis::hset(
                    __METHOD__, self::REDIS_KEY, $memberKey, $data
                );
            }
        }

        foreach ($data as $item)
        {
            $result[] = [
                'date' => [
                    'month' => $item['month'],
                    'year'  => $item['year'],
                ],
                'min' => $item['min_bid'],
                'avg' => $item['avg_bid'],
                'max' => $item['max_bid'],
            ];
        }

        return $result;
    }

    /**
     * Минимум минимумов, максимум максимумов
     *
     * @param int $type_id
     * @param int $category_id
     *
     * @return array
     * @access
     */
    public static function getMinMaxBids(int $type_id, int $category_id): array
    {
        return \Models\BidsGraph::findMinMaxByTypeAndCategory($type_id, $category_id)->toArray();
    }

    /**
     * Добавляет значения ставок для графика
     *
     * Работает по крону
     *
     * @access
     */
    public static function save(): void
    {
        foreach (\Models\BidsGraph::getRawAvgBidsOverPeriod(self::PERIOD_MONTHS) as $item)
        {
            if (is_null($item->avg_bid))
            {
                continue;
            }

            $avg = (int)$item->avg_bid * self::AVG_MULTIPLIER;
            $min = (int)ceil($avg - $avg * (rand(self::MIN_PERCENT, self::MAX_PERCENT) / 100));
            $max = (int)ceil($avg + $avg * (rand(self::MIN_PERCENT, self::MAX_PERCENT) / 100));

            (new \Models\BidsGraph)->fill([
                'type_id' => $item->type_id,
                'category_id' => $item->category_id,
                'month' => $item->month,
                'year' => $item->year,
                'min_bid' => $min,
                'avg_bid' => $avg,
                'max_bid' => $max,
            ])->save();
        }
    }
}