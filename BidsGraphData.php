<?php declare(strict_types=1);

namespace App\GraphQL\Queries\BidsGraph;

use App\GraphQL\Kernel\SingletonPool;
use App\GraphQL\Queries\BaseType;
use GraphQL\Type\Definition\Type;

/**
 * Class BidsGraph
 * Класс для отображения графика ставок для пары тип/предмет
 *
 * @package App\GraphQL\Queries\PerformerTypes\Order
 */
class BidsGraphData extends BaseType
{
    public function __construct()
    {
        $config = [
            'name'        => self::getName(),
            'description' => _('График ставок'),
            'fields'      => [
                'date' => [
                    'type'        => SingletonPool::getInstance(Date::class),
                    'description' => _('Дата ставок. Год, месяц'),
                    'resolve'     => function (array $bidGraph): array {
                        return $bidGraph['date'];
                    },
                ],
                'min'  => [
                    'type'        => Type::int(),
                    'description' => _('Минимальная ставка'),
                    'resolve'     => function (array $bidGraph): int {
                        return (int)$bidGraph['min'];
                    },
                ],
                'avg'  => [
                    'type'        => Type::int(),
                    'description' => _('Средняя ставка'),
                    'resolve'     => function (array $bidGraph): int {
                        return (int)$bidGraph['avg'];
                    },
                ],
                'max'  => [
                    'type'        => Type::int(),
                    'description' => _('Максимальная ставка'),
                    'resolve'     => function (array $bidGraph): int {
                        return (int)$bidGraph['max'];
                    },
                ],
            ],
        ];

        parent::__construct($config);
    }
}