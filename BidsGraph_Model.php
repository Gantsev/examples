<?php declare(strict_types=1);

namespace Models;

use Illuminate\Database\Eloquent\Collection;

/**
 * График ставок для автора
 *
 * Class BidsGraph
 *
 * @package Models
 *
 * @property integer $id
 * @property integer $type_id
 * @property integer $category_id
 * @property integer $month
 * @property integer $year
 * @property integer $min_bid
 * @property integer $avg_bid
 * @property integer $max_bid
 *
 */
class BidsGraph extends Model
{
    public $timestamps = false;

    protected $table = 'bids_graph_data';

    protected $guarded = ['id'];

    protected $casts = [
        'type_id'     => 'integer',
        'category_id' => 'integer',
        'month'       => 'integer',
        'year'        => 'integer',
        'min_bid'     => 'integer',
        'avg_bid'     => 'integer',
        'max_bid'     => 'integer',
    ];

    protected $fillable = [
        'type_id',
        'category_id',
        'month',
        'year',
        'min_bid',
        'avg_bid',
        'max_bid',
    ];

    /**
     * Получаем средние ставки для типа и категории
     *
     * @param int $type_id
     * @param int $category_id
     *
     * @return Collection
     * @access
     */
    public static function findByTypeAndCategory(int $type_id, int $category_id): Collection
    {
        return self::query()
            ->where('type_id', $type_id)
            ->where('category_id', $category_id)
            ->orderBy('year')
            ->orderBy('month')
            ->get();
    }

    /**
     * Получаем минимальную и максимальную ставку для типа и категории
     *
     * @param int $type_id
     * @param int $category_id
     *
     * @return BidsGraph
     * @access
     */
    public static function findMinMaxByTypeAndCategory(int $type_id, int $category_id): self
    {
        return self::query()
            ->selectRaw('MIN(min_bid) AS min_bid, MAX(max_bid) AS max_bid')
            ->where('type_id', $type_id)
            ->where('category_id', $category_id)
            ->get()
            ->first();
    }

    /**
     * Сырые данные для дальнейшей обработки и записи
     *
     * @param int $months
     *
     * @return iterable
     * @access
     */
    public static function getRawAvgBidsOverPeriod(int $months): iterable
    {
        return \EloquentHook::getQueryBuilder()
            ->table('info')
            ->selectRaw(implode(', ', [
                'avg("origin_bid") AS avg_bid',
                'type_id',
                'category_id',
                "date_part('month', creation) AS month",
                "date_part('year', creation) AS year",
            ]))
            ->join('dates', 'dates.order_id', '=', 'info.order_id')
            ->where('creation', '>', \Base_Date::create()->modifyMonths($months)->formatSql())
            ->groupBy(
                'type_id',
                'category_id',
                \EloquentHook::raw("date_part('month', creation)"),
                \EloquentHook::raw("date_part('year', creation)")
            )
            ->get();
    }
}